package karate;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.junit4.Karate;
import org.junit.runner.RunWith;

/* 
 * command line execution
 * 
 * mvn test -Dtest=KarateTests
 * 
 * reference: https://github.com/intuit/karate#command-line
 */

@RunWith(Karate.class)
@KarateOptions(features = {
	    "classpath:karate/newfeature.feature"
		},
		tags = {"~@ignore"})
public class KarateTests
{
}