Feature: Create user
Scenario: Verify that the Post request results in proper status code 200
Given url 'https://reqres.in/api/users'
And request {  "name": "aditya",  "job": "trainer" }
When method post
Then status 201
And print 'the value of response is:  ', response
And print 'the value of id is:  ', response.id

* def id = response.id

And print 'id = ', id