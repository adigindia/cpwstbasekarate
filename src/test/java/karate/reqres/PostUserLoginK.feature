Feature: Post request to URL - https://reqres.in/api/login - User not found
Scenario: Verify that the Post request results in proper status code 200
Given url 'https://reqres.in/api/login'
And request {email: 'peter@klaven', password : 'cityslicka'}
When method post
Then status 400
And print 'The Response is = ', response