Feature: Put request for URL - https://reqres.in/api/users/3
Scenario: Verify for the proper status code and response 
Given url 'https://reqres.in/api/users/3'
And request {name: 'Anand', Job: 'WST'}
When method Put
Then status 200
And print 'the value of response is:', response