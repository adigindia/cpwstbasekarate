Feature: Delete Request for URL - https://reqres.in/ api/users/3
@smoke
Scenario: Verify that the appropriate response is 204
Given url 'https://reqres.in/api/users/12'
When method Delete
Then status 204
And print 'The Response is $$ ', response