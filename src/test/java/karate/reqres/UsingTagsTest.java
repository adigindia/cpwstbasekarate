package karate.reqres;

import org.junit.runner.RunWith;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.junit4.Karate;

// using tags to selectively run features
// reference: https://github.com/intuit/karate/tree/master/karate-demo/src/test/java/demo/tags

@RunWith(Karate.class)

@KarateOptions(tags = "~@smoke")
public class UsingTagsTest {

}
