Feature: Create users from a file

# reference https://github.com/intuit/karate/tree/master/karate-demo/src/test/java/demo/outline

Scenario Outline: Create name: <name> user with a job: <job>

Given url 'https://reqres.in/api/users'
#And print 'Create user for:' + <name> + "with the job = " + <job> 

And request {'name':<name>, 'job':<job>}

When method post

Then status 201

And print 'the value of response is:  ', response

And match response == {"name":<name>,  "job": <job>, "id":'#ignore', "createdAt":'#ignore'}

And match response.id == '#string'

Examples:
| read('./users.json') |

# file can be xml, json, csv 

