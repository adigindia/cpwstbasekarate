package karate.reqres;

import org.junit.runner.RunWith;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.junit4.Karate;

// CreateUserReqRes.feature
// GetRequestReqres.feature
// we can use features options to choose which features or tests to run
// if do not specify features it will run all the features

@RunWith(Karate.class)

@KarateOptions(features = {
	    "classpath:karate/reqres/CreateUserReqRes.feature",
	    "classpath:karate/reqres/CreateUsersScenarioOutline.feature"
		})

public class AllTestsReqRes {

}
