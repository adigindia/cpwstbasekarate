Feature: Get request
Scenario: Verify that GET request on below URL returns status code of 200
Given url 'https://reqres.in/api/users?page=2'
When method get
Then status 200
And print 'the value of response is:  ', response.data
