Feature: Create user and match response

Scenario: Verify that the user is created and the response received is matched

Given url 'https://reqres.in/api/users'

* def inputName = "Aditya Garg"
* def inputJob = "Licensed Trainer"

When request {  "name": '#(inputName)',  "job": '#(inputJob)' }

And method post

Then status 201

And print 'the value of response is:  ', response

And match response == {"name":'#(inputName)',  "job": '#(inputJob)', "id":'#ignore', "createdAt":'#ignore'}

And match response.id == '#string'